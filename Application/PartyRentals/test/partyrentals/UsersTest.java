/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package partyrentals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ahmed Rivaj
 */
public class UsersTest {
    
    static final String env="test";
    
    public UsersTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    /**
     * Test of checkCredentials method for unknown user account, should return 0.
     */
    @Test
    public void testcheckCredentials()throws Exception{
        Users test = new Users();
        test.changeDB(env);
        int result = test.checkCredentials("zlxkdjf03urslkdjfx,mlsjdf0w","64sdfgdtgw5tsdf");
        assertEquals(0,result);;
    }
    
    /**
     * Test of checkCredentials method, should return 1.
     */
    @Test
    public void testReturnCredentials() throws Exception{
        Users test = new Users();
        test.changeDB(env);
        int result = test.checkCredentials("test@gmail.com","welcome123");
        assertEquals(1,result);;
    }




    
}
